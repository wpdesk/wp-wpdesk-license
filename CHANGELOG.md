## [4.0.1] - 2025-01-15
### Added
- wpdesk/license/server to allow to change license server

## [4.0.0] - 2024-09-16
### Added
- Notice on plugin listing, when it is impossible to upgrade the plugin automatically.
### Removed
- A whole bunch of classes and functions related to old upgrade API, relying on direct shop communication.
### Changed
- Added strong return and parameter types.
- Removed direct dependency on `wpdesk/wp-builder` and `wpdesk/wp-notice`.
- Raised minimal PHP version to 7.4.
### Fixed
- PHP warning on referring to missing property when client is blocking external requests.

## [3.5.3] - 2024-04-16
### Fixed
- Avoid PHP warnings on connection failure with license server.

## [3.5.2] - 2024-01-04
### Fixed
- translations

## [3.5.1] - 2023-12-13
### Fixed
- fixed deprecated errors

## [3.5.0] - 2023-08-02
### Added
- wpdesk/license/token filter to allow to change token
- wpdesk/license/use_license filter to switch to license server

## [3.4.4] - 2023-08-02
### Fixed
- array to string notice when no WPDesk logger

## [3.4.3] - 2023-06-13
### Fixed
- message and changelog management
- error behaviour
- upgrade according to new flag need_update

## [3.4.2] - 2023-06-12
### Fixed
- fix to ensure backward compatibility with old activation

## [3.4.1] - 2023-06-12
### Fixed
- urlencode to avoid redirect

## [3.4.0] - 2023-06-12
### Added
- Compatibility with WP Desk license server

### Fixed
- Improved compatibility with PHP >8.0

## [3.3.4] - 2023-05-30

### Fixed
- Improved compatibility with PHP >8.0

## [3.3.3] - 2023-04-19
### Fixed
- Improved Octolize Plugin Detection

## [3.3.2] - 2023-03-29
### Fixed
- Improved compatibility with PHP >8.0

## [3.3.1] - 2023-02-10
### Changed
- Move `wpdesk/wp-logs` to suggested packages.
- Removed arrows from user facing messages

## [3.3.0] - 2022-08-30
### Added
- de_DE translators

## [3.2.2] - 2022-04-19
### Changed
- Support for custom domains urls for license details

## [3.2.1] - 2022-04-12
### Changed
- Hook `upgrader_pre_download` no longer needs 4 arguments for better compatibility with WP <5.5

## [3.2.0] - 2022-03-31
### Changed
- flexibleshipping.com to octolize.com

## [3.1.1] - 2022-03-28
### Fixed
- function get_current_screen not exists

## [3.1.0] - 2022-03-24
### Added
- CTA on expired subscriptions

## [3.0.7] - 2021-11-16
### Fixed
- bail early if no response (error)

## [3.0.6] - 2021-10-01
### Fixed
- escaping

## [3.0.5] - 2021-10-01
### Fixed
- sprintf format

## [3.0.4] - 2021-09-21
### Fixed
- license links

## [3.0.3] - 2021-09-20
### Fixed
- translations
- removed required attribute on key field

## [3.0.2] - 2021-09-20
### Fixed
- disabled integration tests

## [3.0.1] - 2021-09-20
### Changed
- used plugin file instead of plugin slug

## [3.0.0] - 2021-09-01
### Updated
- license manager

## [2.10.3] - 2021-04-06
### Fixed
- debug_backtrace notice in compatibility checker: https://wordpress.org/support/topic/plugin-compatibility-with-php-7-4/

## [2.10.2] - 2021-02-25
### Fixed
- Errors on PHP 8.0

## [2.10.1] - 2020-12-01
### Fixed
- fi.com server can be first for invoices

## [2.10.0] - 2020-12-01
### Added
- fi.com server

## [2.9.0] - 2020-10-15
### Added
- Display details from changelog on plugins list

## [2.8.0] - 2020-03-06
### Added
- unserialize error logging
### Fixed
- sanitization

## [2.7.1] - 2019-11-20
### Fixed
- Invisible subscription menu

## [2.7.0] - 2019-11-13
### Added
- Translations from helper moved here

## [2.6.0] - 2019-11-11
### Added
- ShopMagic servers
### Changed
- Default api domain can vary according to product id

## [2.5.1] - 2019-10-17
### Changed
- Default api domain is wpdesk.pl

## [2.5.0] - 2019-10-09
### Changed
- Plugins can check if activation is available event when subscription is not active
- Show FAQ/Screenshots and other sections only when there is data to shown

## [2.4] - 2019-09-18
### Changed
- Basic classes moved to wp-builder

## [2.3.2] - 2019-08-09
### Fixed
- Notice type

## [2.3.1] - 2019-07-30
### Fixed
- Security for templates as side effect is generated there. Also required for prefixer compatibility

## [2.3.0] - 2019-06-26
### Added
- InfoActivationBuilder with capability to set info if plugin subscription is active

## [2.2.2] - 2019-06-26
### Fixed
- Activation check

## [2.2.1] - 2019-06-13
### Fixed
- Plugin still using old WPDesk_Helper_Plugin sometimes can be shown twice

## [2.2.0] - 2019-06-04
### Added
- wpdesk_show_plugin_activation_notice filter added

## [2.1.0] - 2019-06-04
### Added
- PluginRegistrator - no need for helper
### Changed
- license key -> api key; subscription -> license

## [2.0.2] - 2019-06-03
### Added
- Is using new field in plugin information section: description_base64

## [2.0.1] - 2019-04-30
### Fixed
- Remove call-time-pass-by-reference

## [2.0.0] - 2019-04-17
### Changed
- Renamed to wp-wpdesk-license
- Classes to integrate with helper moved to wp-wpdesk-helper

## [1.1.5] - 2019-04-10
### Fixed
- Fixed fatal error when some clousures are hooked in admin_notices #2

## [1.1.4] - 2019-04-04
### Fixed
- Fixed fatal error when some clousures are hooked in admin_notices

## [1.1.3] - 2019-03-28
### Fixed
- WordPress <5.0 compatibility PB-380

## [1.1.2] - 2019-03-27
### Fixed
- Fixed forced locale in translations PB-349

## [1.1.1] - 2019-03-26
### Fixed
- Fixed helper action removal PB-351

## [1.1.0] - 2019-03-26
### Added
- Supports translations

## [1.0.1] - 2019-03-25
### Fixed
- Can remove info about helper requirement in more ways
- Checks if WPDesk_Logger_Factory exists before using

## [1.0.0] - 2019-03-25
### Added
- Subscriptions
