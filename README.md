[![pipeline status](https://gitlab.com/wpdesk/wp-wpdesk-license/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-wpdesk-license/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-license/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-wpdesk-license/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-license/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-wpdesk-license/commits/master)

wp-wpdesk-license
====================
