<?php

namespace WPDesk\License;

/**
 * @depreacted 4.0.0 Use LicenseServer\PluginRegistrator directly
 */
final class PluginRegistrator extends \WPDesk\License\LicenseServer\PluginRegistrator {
}
