<?php

use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase {
	public function setUp(): void {
		WP_Mock::setUp();
	}

	public function tearDown(): void {
		WP_Mock::tearDown();
	}

	public function testValidChangelog(): void {
		WP_Mock::passthruFunction( 'wp_kses_post' );
		WP_Mock::passthruFunction( 'wp_parse_list' );

		$actual   = $this->getParsedChangelog( $this->getValidChangelog() );
		$expected = array(
			'1.13.0' => array( 'version' => '1.13.0', 'changes' => array( 'Added' => array( 0 => 'Debug mode', ), ), ),
			'1.12.0' => array(
				'version' => '1.12.0',
				'changes' => array( 'Added' => array( 0 => 'Weight rounding on cart contents', ), ),
			),
			'1.11.2' => array(
				'version' => '1.11.2',
				'changes' => array( 'Fixed' => array( 0 => 'Missing package data on order shipping item', ), ),
			),
			'1.11.1' => array(
				'version' => '1.11.1',
				'changes' => array( 'Fixed' => array( 0 => 'Free shipping with various tax settings', ), ),
			),
			'1.11.0' => array(
				'version' => '1.11.0',
				'changes' => array(
					'Added' => array( 0 => 'Free shipping option: Apply minimum order rule before coupon discount', ),
					'Fixed' => array( 0 => 'Flexible Shipping requirements and install links', ),
				),
			),
		);

		$this->assertEquals( $expected, $actual );
	}

	public function testInvalidChangelog(): void {
		WP_Mock::passthruFunction( 'wp_kses_post' );
		WP_Mock::passthruFunction( 'wp_parse_list' );

		$actual   = $this->getParsedChangelog( $this->getInvalidChangelog() );
		$expected = array();

		$this->assertEquals( $expected, $actual );
	}

	/**
	 * @param string $changelog
	 *
	 * @return array
	 */
	private function getParsedChangelog( string $changelog ): array {
		$changelog = base64_encode( $changelog );
		$parser    = new WPDesk\License\Changelog\Parser( $changelog );
		$parser->parse();

		return $parser->get_parsed_changelog()->getIterator()->getArrayCopy();
	}

	private function getValidChangelog(): string {
		return '*** Flexible Shipping PRO Changelog ***

## [1.13.0] - 2020-09-28
### Added
- Debug mode

## [1.12.0] - 2020-09-09
### Added
- Weight rounding on cart contents

## [1.11.2] - 2020-07-31
### Fixed
- Missing package data on order shipping item

## [1.11.1] - 2020-05-28
### Fixed
- Free shipping with various tax settings

## [1.11.0] - 2020-05-25
### Added
- Free shipping option: Apply minimum order rule before coupon discount
### Fixed
- Flexible Shipping requirements and install links

##### Added,Fixed';
	}

	private function getInvalidChangelog(): string {
		return '*** WooCommerce Paczkomaty Changelog ***

3.15.0 - 2020-09-01
* Zmiana usługi Allegro MiniPaczka na Allegro miniKurier24
* Dodanie usługi Allegro miniKurier24 za pobraniem
* Poprawiono obliczanie szerokości Select2 podczas wyszukiwania Paczkomatów

3.14.0 - 2020-08-06
* Usunięcie z mapy paczkomatów niedostępnych Punktów Odbioru Przesyłki
* Aktualizacja związana ze zmianami w API InPost: https://inpost.pl/aktualnosci-api-points
* Poprawione obliczanie wagi zamówienia z przecinkiem jako separatorem dziesiętnym
* Dodanie dynamicznego wyszukiwania paczkomatów w ustawieniach InPost
* Dodanie dynamicznego wyszukiwania paczkomatów podczas przygotowywania przesyłki
* Poprawienie obsługi mapy paczkomatów
* Dodanie możliwości nadania przesyłki kurierskiej w Paczkomacie (inpost_courier_c2c)';
	}
}
